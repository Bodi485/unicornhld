<?php get_header(); ?>
<div class="background-contacts" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/contacts/background-contacts.png'); ?>)">
    <div class="background-gradient"></div>
    <div class="basic-container">
        <div class="contacts-container">
            <h2 class="section-title">
                <?php if (get_field('offices_title')): ?>                
                    <?php the_field('offices_title'); ?>   
                <?php endif; ?> 
            </h2>
            <div class="flex-container-contacts">
                <div class="flex-container-adress">
                    <div><p class="country-text">
                            <?php if (get_field('offices_ukraine')): ?>                
                                <?php the_field('offices_ukraine'); ?>   
                            <?php endif; ?> 
                        </p></div>
                    <div class="contacts-items">
                        <div class="contacts-images" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/location.png'); ?>)"></div>
                        <div><p class="contacts-text">
                                <?php if (get_field('offices_ukraine_adress')): ?>                
                                    <?php the_field('offices_ukraine_adress'); ?>   
                                <?php endif; ?> 
                            </p></div>
                    </div>
                    <div class="contacts-items">
                        <div class="contacts-images" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/call.png'); ?>)"></div>
                        <div><p class="contacts-text contacts-text-padding">
                                <?php if (get_field('offices_ukraine_phone')): ?>                
                                    <?php the_field('offices_ukraine_phone'); ?>   
                                <?php endif; ?> 
                            </p></div>
                    </div>
                </div>
                <div class="flex-map">
                    <?php if (get_field('offices_ukraine_map')): ?>                
                    <a href="<?php if (get_field('offices_ukraine_map_link')){ the_field('offices_ukraine_map_link');} ?>" target="_blank"><div class="maps" style="background-image: url(<?php the_field('offices_ukraine_map'); ?>  )"></div> </a>
                    <?php endif; ?> 
                </div>
            </div>

            <div class="flex-container-contacts">
                <div class="flex-container-adress">
                    <div><p class="country-text">
                            <?php if (get_field('offices_belarus')): ?>                
                                <?php the_field('offices_belarus'); ?>   
                            <?php endif; ?> 
                        </p></div>
                    <div class="contacts-items">
                        <div class="contacts-images" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/location.png'); ?>)"></div>
                        <div><p class="contacts-text">
                                <?php if (get_field('offices_belarus_adress')): ?>                
                                    <?php the_field('offices_belarus_adress'); ?>   
                                <?php endif; ?> 
                            </p></div>
                    </div>

                    <div class="contacts-items">
                        <div class="contacts-images" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/call.png'); ?>)"></div>
                        <div><p class="contacts-text contacts-text-padding">
                                <?php if (get_field('offices_belarus_phone')): ?>                
                                    <?php the_field('offices_belarus_phone'); ?>   
                                <?php endif; ?> 
                            </p></div>
                    </div>
                </div>
                <div class="flex-map">
                    <?php if (get_field('offices_belarus_map')): ?>                
                    <a href="<?php if (get_field('offices_belarus_map_link')){ the_field('offices_belarus_map_link');} ?>" target="_blank"><div class="maps" style="background-image: url(<?php the_field('offices_belarus_map'); ?>  )"></div></a>
                    <?php endif; ?> 
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>