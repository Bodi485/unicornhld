<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>  
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo esc_url(get_template_directory_uri()); ?>/favicon.ico" type="image/x-icon" />
</head>
<body>

<!---------------HEADER START----------------->
<div class="page-wrapper">
    <header class="header header-container header-clear-padding">
        <a href="<?php echo home_url(); ?>" class="logo-link">
            <div class="logo" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/logo.png'); ?>);"></div>
        </a>
        <div class="icon-container" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/menu.png'); ?>);"></div>
        <div class="background-title-page"></div>
       <div class="navbar">
            <span class="btn-close">&times;</span>
            <?php nav(); ?> 
        </div>
        <label class="custom-select">
            <?php pll_the_languages(array('dropdown'=>1,'display_names_as'=>0));  ?>
        </label>
    </header>
    <!---------------HEADER END------------------->