<?php get_header(); ?>

<h1><?php the_title(); ?></h1>

<section>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile;
    endif; ?>
</section>
<?php get_footer(); ?>