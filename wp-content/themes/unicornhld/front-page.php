<?php get_header(); ?>
<?php if (get_field('main_image')): ?>
    <section class="main-image-container">
        <div class="main-image-wrapper">
            <div class="main-image" style="background-image: url(<?php the_field('main_image'); ?>);">
                <span class="main-subtitle">
                    <?php if (get_field('main_image_text')): ?>                
                        <?php the_field('main_image_text'); ?>   
                    <?php endif; ?> 
                </span>
            </div>
        </div>
        <p class="section-text">
            <?php if (get_field('main_image_desc')): ?>                
                <?php the_field('main_image_desc'); ?>   
            <?php endif; ?> 
        </p>
    </section>
<?php endif; ?>
<!---------------MAIN CONTENT START----------------->
<div class="business-main-container">
    <h2 class="section-title business-title-margin"><?php pll_e('BUSINESS'); ?></h2>

    <div class="business-container">
        <h3 class="business-title">
            <?php if (get_field('multi_development_title')): ?>
                <?php the_field('multi_development_title'); ?>
            <?php endif; ?>
        </h3>
        <div class="swiper-container-business">
            <div class="swiper-wrapper">
                <?php if (have_rows('multi_development_images')): ?> 
                    <?php while (have_rows('multi_development_images')) : the_row(); ?> 
                        <div class="swiper-slide swiper-slide-business">
                            <?php if (get_sub_field('multi_development_image')): ?>
                                <div class="business-image-wrapper"
                                     style="background-image: url('<?php the_sub_field('multi_development_image'); ?>')">
                                </div>
                            <?php endif; ?>
                            <div class="business-chevron"></div>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </div>
        <div class="slide-counter">
            <div class="slide-counter-wrap">
                <div class="swiper-button-prev-bus swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                <div class="slide-counter-number">
                    <div class="swiper-pagination-0"></div>
                </div>
                <div class="swiper-button-next-bus swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
            </div>
        </div>
        <div class="business-chevron-descktop"></div>
        <div class="business-details">
            <?php if (have_rows('multi_development_details')): ?> 
                <?php while (have_rows('multi_development_details')) : the_row(); ?> 
                    <div class="business-details-item">
                        <p class="business-details-item-title">
                            <?php if (get_sub_field('multi_development_details_title')): ?>
                                <?php the_sub_field('multi_development_details_title'); ?>
                            <?php endif; ?>
                        </p>
                        <p class="business-details-item-description">
                            <?php if (get_sub_field('multi_development_details_desc')): ?>
                                <?php the_sub_field('multi_development_details_desc'); ?>
                            <?php endif; ?>
                        </p>
                    </div>
                <?php endwhile; ?> 
            <?php endif; ?> 
        </div>
        <div class="business-option-wrapper">
            <p class="business-option-description">
                <?php if (get_field('multi_development_desc')): ?>
                    <?php the_field('multi_development_desc'); ?>
                <?php endif; ?>
            </p>
            <button class="business-option-button">
                <?php if (get_field('multi_development_link')): ?>
                    <a href="<?php the_field('multi_development_link'); ?>" class="business-option-button-link">
                        <?php pll_e('More'); ?>
                    </a>
                <?php endif; ?>
            </button>
        </div>
    </div>
    <div class="business-container business-container-margin">
        <h3 class="business-title">
            <?php if (get_field('it_education_title')): ?>
                <?php the_field('it_education_title'); ?>
            <?php endif; ?>
        </h3>
        <div class="swiper-container-education">
            <div class="swiper-wrapper">
                <?php if (have_rows('it_education_images')): ?> 
                    <?php while (have_rows('it_education_images')) : the_row(); ?> 
                        <div class="swiper-slide swiper-slide-business">
                            <?php if (get_sub_field('it_education_image')): ?>
                                <div class="business-image-wrapper"
                                     style="background-image: url('<?php the_sub_field('it_education_image'); ?>')">
                                </div>
                            <?php endif; ?>
                            <div class="business-chevron-right"></div>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </div>
        <div class="slide-counter">
            <div class="slide-counter-wrap">
                <div class="swiper-button-prev-ed swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                <div class="slide-counter-number">
                    <div class="swiper-pagination-1"></div>
                </div>
                <div class="swiper-button-next-ed swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)">
                </div>
            </div>
        </div>
        <div class="business-chevron-descktop"></div>
        <div class="business-details">
            <?php if (have_rows('it_education_details')): ?> 
                <?php while (have_rows('it_education_details')) : the_row(); ?> 
                    <div class="business-details-item">
                        <p class="business-details-item-title">
                            <?php if (get_sub_field('it_education_details_title')): ?>
                                <?php the_sub_field('it_education_details_title'); ?>
                            <?php endif; ?>
                        </p>
                        <p class="business-details-item-description">
                            <?php if (get_sub_field('it_education_details_desc')): ?>
                                <?php the_sub_field('it_education_details_desc'); ?>
                            <?php endif; ?>
                        </p>
                    </div>
                <?php endwhile; ?> 
            <?php endif; ?> 
        </div>
        <div class="business-option-wrapper">
            <p class="business-option-description">
                <?php if (get_field('it_education_desc')): ?>
                    <?php the_field('it_education_desc'); ?>
                <?php endif; ?>
            </p>
            <button class="business-option-button">
                <?php if (get_field('it_education_link')): ?>
                    <a href="<?php the_field('it_education_link'); ?>" class="business-option-button-link">
                        <?php pll_e('More'); ?>
                    </a>
                <?php endif; ?>
            </button>
        </div>
    </div>

    <div class="business-container business-container-margin">
        <h3 class="business-title">
            <?php if (get_field('sport_title')): ?>
                <?php the_field('sport_title'); ?>
            <?php endif; ?>
        </h3>
        <div class="swiper-container-sport">
            <div class="swiper-wrapper">
                <?php if (have_rows('sport_images')): ?> 
                    <?php while (have_rows('sport_images')) : the_row(); ?> 
                        <div class="swiper-slide swiper-slide-business">
                            <?php if (get_sub_field('sport_image')): ?>
                                <div class="business-image-wrapper"
                                     style="background-image: url('<?php the_sub_field('sport_image'); ?>')">
                                </div>
                            <?php endif; ?>
                            <div class="business-chevron"></div>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </div>

        <div class="slide-counter">
            <div class="slide-counter-wrap">
                <div class="swiper-button-prev-sp swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                <div class="slide-counter-number">
                    <div class="swiper-pagination-2"></div>
                </div>
                <div class="swiper-button-next-sp swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
            </div>
        </div>
        <div class="business-chevron-descktop"></div>
        <div class="business-details">
            <?php if (have_rows('sport_details')): ?> 
                <?php while (have_rows('sport_details')) : the_row(); ?> 
                    <div class="business-details-item">
                        <p class="business-details-item-title">
                            <?php if (get_sub_field('sport_details_title')): ?>
                                <?php the_sub_field('sport_details_title'); ?>
                            <?php endif; ?>
                        </p>
                        <p class="business-details-item-description">
                            <?php if (get_sub_field('sport_details_desc')): ?>
                                <?php the_sub_field('sport_details_desc'); ?>
                            <?php endif; ?>
                        </p>
                    </div>
                <?php endwhile; ?> 
            <?php endif; ?> 
        </div>
        <div class="business-option-wrapper">
            <p class="business-option-description">
                <?php if (get_field('sport_desc')): ?>
                    <?php the_field('sport_desc'); ?>
                <?php endif; ?>
            </p>
            <button class="business-option-button">
                <?php if (get_field('sport_link')): ?>
                    <a href="<?php the_field('sport_link'); ?>" class="business-option-button-link">
                        <?php pll_e('More'); ?>
                    </a>
                <?php endif; ?>
            </button>
        </div>
    </div>
    <div class="business-container business-container-margin">
        <h3 class="business-title">
            <?php if (get_field('marketing_title')): ?>
                <?php the_field('marketing_title'); ?>
            <?php endif; ?>
        </h3>
        <div class="swiper-container-marketing">
            <div class="swiper-wrapper">
                <?php if (have_rows('marketing_images')): ?> 
                    <?php while (have_rows('marketing_images')) : the_row(); ?> 
                        <div class="swiper-slide swiper-slide-business">
                            <?php if (get_sub_field('marketing_image')): ?>
                                <div class="business-image-wrapper"
                                     style="background-image: url('<?php the_sub_field('marketing_image'); ?>')">
                                </div>
                            <?php endif; ?>
                            <div class="business-chevron-right"></div>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </div>
        <div class="slide-counter">
            <div class="slide-counter-wrap">
                <div class="swiper-button-prev-mark swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                <div class="slide-counter-number">
                    <div class="swiper-pagination-3"></div>
                </div>
                <div class="swiper-button-next-mark swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
            </div>
        </div>
        <div class="business-chevron-descktop"></div>
        <div class="business-details">
            <?php if (have_rows('marketing_details')): ?> 
                <?php while (have_rows('marketing_details')) : the_row(); ?> 
                        <div class="business-details-item">
                            <p class="business-details-item-title">
                                <?php if (get_sub_field('marketing_details_title')): ?>
                                    <?php the_sub_field('marketing_details_title'); ?>
                                <?php endif; ?>
                            </p>
                            <p class="business-details-item-description">
                                <?php if (get_sub_field('marketing_details_desc')): ?>
                                    <?php the_sub_field('marketing_details_desc'); ?>
                                <?php endif; ?>
                            </p>
                        </div>
                <?php endwhile; ?> 
            <?php endif; ?> 
        </div>
        <div class="business-option-wrapper">
            <p class="business-option-description">
                <?php if (get_field('marketing_desc')): ?>
                    <?php the_field('marketing_desc'); ?>
                <?php endif; ?>
            </p>
            <button class="business-option-button">
                <?php if (get_field('marketing_link')): ?>
                    <a href="<?php the_field('marketing_link'); ?>" class="business-option-button-link">
                        <?php pll_e('More'); ?>
                    </a>
                <?php endif; ?>
            </button>
        </div>
    </div>
</div>
<!---------------MAIN CONTENT END----------------->
<!---------------TEAM SECTION START----------------->
<section class="team basic-container">
    <div class="team-section-wrapper">
        <h2 class="section-title"><?php pll_e('Team'); ?></h2>
        <p class="section-text text-margin">
            <?php if (get_field('team_desc')): ?>
                <?php the_field('team_desc'); ?>
            <?php endif; ?>
        </p>

        <!---------------DESCTOP TEAM VIEW START----------------->
        <div class="team-container">
            <?php if (have_rows('team_persons')): ?> 
                <?php while (have_rows('team_persons')) : the_row(); ?> 
                    <div class="person-wrapper">
                        <?php if (get_sub_field('team_person_img')): ?>
                            <div class="person-picture" style="background-image: url(<?php the_sub_field('team_person_img'); ?>)"></div>
                        <?php endif; ?>
                        <div class="person-details">
                            <p class="person-dept person-item">
                                <?php if (get_sub_field('team_person_title')): ?>
                                    <?php the_sub_field('team_person_title'); ?>
                                <?php endif; ?>
                            </p>
                            <p class="person-name person-item">
                                <?php if (get_sub_field('team_person_name')): ?>
                                    <?php the_sub_field('team_person_name'); ?>
                                <?php endif; ?>
                            </p>
                            <p class="person-position person-item">
                                <?php if (get_sub_field('team_person_position')): ?>
                                    <?php the_sub_field('team_person_position'); ?>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>
                <?php endwhile; ?> 
            <?php endif; ?> 
        </div>
        <!---------------DESCTOP TEAM VIEW END----------------->
        <!---------------SWIPER START----------------->
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php if (have_rows('team_persons')): ?> 
                    <?php while (have_rows('team_persons')) : the_row(); ?> 
                        <div class="swiper-slide person-wrapper">
                            <?php if (get_sub_field('team_person_img')): ?>
                                <div class="person-picture" style="background-image: url(<?php the_sub_field('team_person_img'); ?>)"></div>
                            <?php endif; ?>
                            <div class="person-details">
                                <p class="person-dept person-item">
                                    <?php if (get_sub_field('team_person_title')): ?>
                                        <?php the_sub_field('team_person_title'); ?>
                                    <?php endif; ?>
                                </p>
                                <p class="person-name person-item">
                                    <?php if (get_sub_field('team_person_name')): ?>
                                        <?php the_sub_field('team_person_name'); ?>
                                    <?php endif; ?>
                                </p>
                                <p class="person-position person-item">
                                    <?php if (get_sub_field('team_person_position')): ?>
                                        <?php the_sub_field('team_person_position'); ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>

            <!-- Add Arrows -->
            <div class="swiper-button-next arrow arrow-right" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/team/Arrow_right.png'); ?>)"></div>
            <div class="swiper-button-prev arrow arrow-left" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/team/Arrow_left.png'); ?>)"></div>
        </div>
        <!---------------SWIPER END----------------->
    </div>
</section>

<?php get_footer(); ?>