<?php

remove_filter('the_content', 'wpautop');

add_action('wp_enqueue_scripts', 'theme_name_style');

function theme_name_style() {
    wp_enqueue_style('style-name0', 'https://fonts.googleapis.com/css?family=Montserrat:100,300,400,500,700&display=swap&subset=cyrillic-ext');
    wp_enqueue_style('style-name2', get_template_directory_uri() . '/css/swiper.min.css');
    wp_enqueue_style('style-name1', get_template_directory_uri() . '/css/style.css');
}

// отключение ненужных теме стилей и скриптов begin
//function my_deregister_styles_and_scripts() {
//    wp_dequeue_style('wp-block-library');
//    wp_deregister_style ('contact-form-7');	
//}
//add_action( 'wp_print_styles', 'my_deregister_styles_and_scripts', 100 );
// отключение ненужных теме стилей и скриптов end

add_action('wp_enqueue_scripts', my_update_jquery);

// Remove themes old version of jQuery and load a compatible version
function my_update_jquery() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', '', '', 'in_footer');
        wp_enqueue_script('jquery');
    }
}

add_action('wp_enqueue_scripts', 'wptuts_scripts_with_jquery');

function wptuts_scripts_with_jquery() {
    if (!is_admin()) {
        wp_enqueue_script('custom-script2', get_template_directory_uri() . '/js/swiper.min.js', array('jquery'), '', 'in_footer');
        wp_enqueue_script('custom-script1', get_template_directory_uri() . '/js/index.js', array('jquery'), '', 'in_footer');
        if (is_front_page()) {
            wp_enqueue_script('custom-script3', get_template_directory_uri() . '/js/title-page.js', array('jquery'), '', 'in_footer');
        } else {
            wp_enqueue_script('custom-script4', get_template_directory_uri() . '/js/real-estate.js', array('jquery'), '', 'in_footer');
        }
        wp_enqueue_script('custom-script5', get_template_directory_uri() . '/js/b-js.js', array('jquery'), '', 'in_footer');
    }
}

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'top-menu' => 'top-menu',
            )
    );
}

function nav() {
    wp_nav_menu(
            array(
                'theme_location' => 'top-menu',
                'menu' => '',
                'container' => '',
                'container_class' => '',
                'container_id' => '',
                'menu_class' => 'menu-items',
                'menu_id' => 'top-menu',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'before' => '',
                'after' => '',
                'link_before' => '',
                'link_after' => '',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => 0,
                'walker' => ''
            )
    );
}

// Class for li submetu
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function special_nav_class($classes, $item) {
    if ($item->title == "Бизнесы" || $item->title == "Business") {
        $classes[] = "menu-item menu-item-dropdown";
    } else {
        $classes[] = "menu-item";
    }
    return $classes;
}

// Add Class to <a> of li nav menu
add_filter('nav_menu_link_attributes', 'add_class_to_all_menu_links', 10, 2);

function add_class_to_all_menu_links($atts, $item) {
    if ($item->title == "Мультиформатный девелопмент" || $item->title == "IT-образование" || $item->title == "Спорт" || $item->title == "Маркетинг" ||
            $item->title == "Multi-format development" || $item->title == "IT education" || $item->title == "Sport" || $item->title == "Marketing"
    ) {
        $atts['class'] = 'dropdown-item-link';
    } else {
        $atts['class'] = 'menu-link';
    }

    return $atts;
}

// Change sub-menu class to a custom one
add_filter('wp_nav_menu', 'change_submenu_class');

function change_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/', '/ class="dropdown" /', $menu);
    return $menu;
}

add_filter('acf/options_page/settings', 'my_acf_options_page_settings');

function my_acf_options_page_settings($settings) {
    $settings['title'] = 'Option';
    $settings['pages'] = array('General');
    return $settings;
}

add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});


add_action('init', function() {
    pll_register_string('BUSINESS', 'BUSINESS');
    pll_register_string('Team', 'Team');
    pll_register_string('More', 'More');
    pll_register_string('ARCHITERTURAL_ALLIANCE', 'ARCHITERTURAL_ALLIANCE');
    pll_register_string('Site', 'Site');
	pll_register_string('Contacts', 'Contacts');
});
