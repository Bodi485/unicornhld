<?php get_header(); ?>
<div class="basic-container page-wrapper">
    <section class="notfoundpage" style="text-align: center; color:#fff; margin:150px 0;">
        <div class="notfound">
            <h1 style="font-size: 40px; margin-bottom: 20px;">Page not found</h1>
            <p><a href="<?php echo home_url(); ?>" >Back to home</a></p>
        </div>
    </section>
</div>
<?php get_footer(); ?>