// dropdown menu for desktop
const menuItemDropdown = document.querySelector('.menu-item-dropdown');
const dropdown = document.querySelector('.dropdown');


menuItemDropdown.addEventListener('click', () => {
    dropdown.classList.contains('dropdown-active')
        ? dropdown.classList.remove('dropdown-active')
        : dropdown.classList.add('dropdown-active');
});


// sidenavbar menu for mobile
const barIcon = document.querySelector('.icon-container');
const closeIcon = document.querySelector('.btn-close');
const pageBackground = document.querySelector('.background-title-page');


barIcon.addEventListener('click', () => {
    document.querySelector('.navbar').classList.add('opened-navbar');
    pageBackground.classList.add('background-title-page-appear');
});

closeIcon.addEventListener('click', () => {
    document.querySelector('.navbar').classList.remove('opened-navbar');
    pageBackground.classList.remove('background-title-page-appear')
});


