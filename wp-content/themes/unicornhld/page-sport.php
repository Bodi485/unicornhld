<?php get_header(); ?>
<div class="basic-container page-wrapper">
    <div class="basic-container">
        <?php if (get_field('sport_main_image')): ?>
            <section class="title-image-section">
                <div class="main-image-wrapper pink">
                    <div class="main-image main-image-black centered"
                         style="background-image: url(<?php the_field('sport_main_image'); ?>);">
                        <div class="main-title">
                            <p>
                                <?php if (get_field('sport_main_image_title')): ?>                
                                    <?php the_field('sport_main_image_title'); ?>   
                                <?php endif; ?> 
                            </p>
                            <?php if (get_field('sport_main_image_link')): ?>  
                                <a href="<?php the_field('sport_main_image_link'); ?>" class="main-link"><?php pll_e('Site'); ?></a>
                            <?php endif; ?> 
                        </div>
                    </div>
                </div>
                <p class="section-text">
                    <?php if (get_field('sport_main_image_desc')): ?>                
                        <?php the_field('sport_main_image_desc'); ?>   
                    <?php endif; ?> 
                </p>
            </section>
        <?php endif; ?>
        <section>
            <div class="gym-wrapper">
                <?php if (have_rows('sport_items')): ?> 
                    <?php while (have_rows('sport_items')) : the_row(); ?> 
                        <div class="gym-item">
                            <?php if (get_sub_field('sport_items_img')): ?>
                                <div class="gym-image" style="background-image: url(<?php the_sub_field('sport_items_img'); ?>);"></div>
                            <?php endif; ?> 
                            <p class="gym-text">
                                <?php if (get_sub_field('sport_items_text')): ?>
                                    <?php the_sub_field('sport_items_text'); ?>
                                <?php endif; ?> 
                            </p>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </section>
        <!-------------- ICON SECTION ----------------->
        <section class="basic-container">
            <div class="icon-wrapper">
                <?php if (have_rows('sport_icons')): ?> 
                    <?php while (have_rows('sport_icons')) : the_row(); ?> 
                        <div class="icon-item">
                            <?php if (get_sub_field('sport_icons_img')): ?>
                                <div class="icon-img" style="background-image:url(<?php the_sub_field('sport_icons_img'); ?>);background-repeat: no-repeat;background-size: 100% 100%;"></div>
                            <?php endif; ?> 
                            <p class="icon-info">
                                <?php if (get_sub_field('sport_icons_text')): ?>
                                    <?php the_sub_field('sport_icons_text'); ?>
                                <?php endif; ?> 
                            </p>
                        </div>
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </section>
        <!------------------- MAP ---------------------->

        <?php if (get_field('sport_map_image')): ?>
            <a href="<?php if (get_field('sport_map_image_link')){ the_field('sport_map_image_link');} ?>" target="_blank">
            <section class="map-section">
                <div class="map-wrapper energym-map-wrap">
                   <div class="map" style="background-image:url(<?php the_field('sport_map_image'); ?>)"></div>
                    <p class="map-description">
                        <?php if (get_field('sport_map_image_text')): ?>
                            <?php the_field('sport_map_image_text'); ?>
                        <?php endif; ?> 
                    </p>
                </div>
            </section>
            </a>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>