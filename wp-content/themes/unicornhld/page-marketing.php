<?php get_header(); ?>
<div class="basic-container page-wrapper">
    <?php if (get_field('marketing_main_image')): ?>
        <section class="title-image-section">
            <div class="main-image-wrapper violet">
                <div class="main-image main-image-black centered"
                     style="background-image: url(<?php the_field('marketing_main_image'); ?>);">
                    <div class="main-title">
                        <p>
                            <?php if (get_field('marketing_main_image_title')): ?>                
                                <?php the_field('marketing_main_image_title'); ?>   
                            <?php endif; ?> 
                        </p>
                        <?php if (get_field('marketing_main_image_link')): ?>  
                            <a href="<?php the_field('marketing_main_image_link'); ?>" class="main-link"><?php pll_e('Site'); ?></a>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
            <p class="section-text">
                <?php if (get_field('marketing_main_image_desc')): ?>                
                    <?php the_field('marketing_main_image_desc'); ?>   
                <?php endif; ?> 
            </p>
        </section>
    <?php endif; ?>
    <section>
	
	<h2 class="section-title">
		<?php if (get_field('marketing_icon_title')): ?>                
			<?php the_field('marketing_icon_title'); ?>   
		<?php endif; ?> 
	</h2>
	
        <div class="icon-wrapper">
            <?php if (have_rows('marketing_icons')): ?> 
                <?php while (have_rows('marketing_icons')) : the_row(); ?> 
                    <div class="icon-item">
                        <?php if (get_sub_field('marketing_icons_img')): ?>
                            <div class="icon-img" style="background-image:url(<?php the_sub_field('marketing_icons_img'); ?>)"></div>
                        <?php endif; ?> 
                        <p class="icon-info">
                            <?php if (get_sub_field('marketing_icons_text')): ?>
                                <?php the_sub_field('marketing_icons_text'); ?>
                            <?php endif; ?> 
                        </p>
                    </div>
                <?php endwhile; ?> 
            <?php endif; ?> 
        </div>
    </section>
    <?php if (get_field('marketing_map_image')): ?>
        <section class="map-section">
            <div class="map-wrapper marketing-map-wrap">
                <a href="<?php if (get_field('marketing_map_image_link')){ the_field('marketing_map_image_link');} ?>" target="_blank"><div class="map marketing-map" style="background-image:url(<?php the_field('marketing_map_image'); ?>)"></div></a>
            </div>
        </section>
    <?php endif; ?>
</div>
<?php get_footer(); ?>