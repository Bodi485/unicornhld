<?php get_header(); ?>
<div class="basic-container page-wrapper">
    <?php if (get_field('mfd_main_image')): ?>
        <section>
            <div class="main-image-wrapper">
                <div class="main-image main-image-black centered"
                     style="background-image: url(<?php the_field('mfd_main_image'); ?>);">
                    <div class="main-title main-title-smaller estate-main-title">
                        <p>
                            <?php if (get_field('mfd_main_image_text')): ?>                
                                <?php the_field('mfd_main_image_text'); ?>   
                            <?php endif; ?> 
                        </p>
                    </div>
                </div>
            </div>
            <p class="section-text"> </p>
            <p class="section-text"> </p>
        </section>
    <?php endif; ?>
    <section>
        <!--------------OFFICE BUILDING SILVER BREEZE----------------->
        <div class="business-main-container">

            <h1 class="section-title">
                <?php if (get_field('mfd_ukr_title')): ?>                
                    <?php the_field('mfd_ukr_title'); ?>   
                <?php endif; ?> 
            </h1>

            <div class="business-container">
                <p class="business-title">
                    <?php if (get_field('mfd_ukr_firstblock_title')): ?>                
                        <?php the_field('mfd_ukr_firstblock_title'); ?>   
                    <?php endif; ?> 
                </p>
                <div class="swiper-container-office1">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_ukr_firstblock_images')): ?> 
                            <?php while (have_rows('mfd_ukr_firstblock_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_ukr_firstblock_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_ukr_firstblock_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>

                <div class="slide-counter">
                    <div class="slide-counter-wrap">
                        <div class="swiper-button-prev-off1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)">
                        </div>
                        <div class="slide-counter-number">
                            <div class="swiper-pagination-real-0"></div>
                        </div>
                        <div class="swiper-button-next-off1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)">
                        </div>
                    </div>
                </div>

                <div class="business-chevron-descktop"></div>
                <div class="business-details">
                    <?php if (have_rows('mfd_ukr_firstblock_details')): ?> 
                        <?php while (have_rows('mfd_ukr_firstblock_details')) : the_row(); ?> 
                            <div class="business-details-item cols2">
                                <p class="business-details-item-title">
                                    <?php if (get_sub_field('mfd_ukr_firstblock_details_title')): ?>
                                        <?php the_sub_field('mfd_ukr_firstblock_details_title'); ?>
                                    <?php endif; ?>
                                </p>
                                <p class="business-details-item-description">
                                    <?php if (get_sub_field('mfd_ukr_firstblock_details_desc')): ?>
                                        <?php the_sub_field('mfd_ukr_firstblock_details_desc'); ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        <?php endwhile; ?> 
                    <?php endif; ?> 
                </div>

                <div class="business-option-wrapper">
                    <p class="business-option-description immovables">
                        <?php if (get_field('mfd_ukr_firstblock_desc')): ?>
                            <?php the_field('mfd_ukr_firstblock_desc'); ?>
                        <?php endif; ?>
                    </p>
                </div>
            </div>

        </div>
        <?php if (get_field('mfd_ukr_firstblock_map_image')): ?>
        <a href="<?php if (get_field('mfd_ukr_firstblock_map_image_link')){ the_field('mfd_ukr_firstblock_map_image_link');} ?>" target="_blank">
            <section class="map-section">
                <div class="map-wrapper">
                    <div class="map" style="background-image:url(<?php the_field('mfd_ukr_firstblock_map_image'); ?>)"></div>
                    <p class="map-description">
                        <?php if (get_field('mfd_ukr_firstblock_map_text')): ?>                
                            <?php the_field('mfd_ukr_firstblock_map_text'); ?>   
                        <?php endif; ?> 
                    </p>
                </div>
            </section>
        </a>
        <?php endif; ?>
        <?php if (get_field('mfd_ukr_secondblock_title')): ?>  
            <div class="business-main-container">
                <div class="real-estate-description"><p><?php pll_e('ARCHITERTURAL_ALLIANCE'); ?></p></div>

                <div class="business-container">
                    <p class="business-title">

                        <?php the_field('mfd_ukr_secondblock_title'); ?>   

                    </p>
                    <div class="swiper-container-office1">
                        <div class="swiper-wrapper">
                            <?php if (have_rows('mfd_ukr_secondblock_images')): ?> 
                                <?php while (have_rows('mfd_ukr_secondblock_images')) : the_row(); ?> 
                                    <div class="swiper-slide swiper-slide-business">
                                        <?php if (get_sub_field('mfd_ukr_secondblock_image')): ?>
                                            <div class="business-image-wrapper"
                                                 style="background-image: url('<?php the_sub_field('mfd_ukr_secondblock_image'); ?>')">
                                            </div>
                                        <?php endif; ?> 
                                        <div class="business-chevron"></div>
                                    </div>
                                <?php endwhile; ?> 
                            <?php endif; ?> 
                        </div>
                    </div>

                    <div class="slide-counter">
                        <div class="slide-counter-wrap">
                            <div class="swiper-button-prev-off1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)">
                            </div>
                            <div class="slide-counter-number">
                                <div class="swiper-pagination-real-0"></div>
                            </div>
                            <div class="swiper-button-next-off1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)">
                            </div>
                        </div>
                    </div>

                    <div class="business-chevron-descktop"></div>
                    <div class="business-details">
                        <?php if (have_rows('mfd_ukr_secondblock_details')): ?> 
                            <?php while (have_rows('mfd_ukr_secondblock_details')) : the_row(); ?> 
                                <div class="business-details-item cols2">
                                    <p class="business-details-item-title">
                                        <?php if (get_sub_field('mfd_ukr_secondblock_details_title')): ?>
                                            <?php the_sub_field('mfd_ukr_secondblock_details_title'); ?>
                                        <?php endif; ?>
                                    </p>
                                    <p class="business-details-item-description">
                                        <?php if (get_sub_field('mfd_ukr_secondblock_details_desc')): ?>
                                            <?php the_sub_field('mfd_ukr_secondblock_details_desc'); ?>
                                        <?php endif; ?>
                                    </p>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>

                    <div class="business-option-wrapper" style="top: 35%">
                        <p class="business-option-description immovables" style="height: 237px;">
                            <?php if (get_field('mfd_ukr_secondblock_desc')): ?>
                                <?php the_field('mfd_ukr_secondblock_desc'); ?>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php endif; ?>  
        <?php if (get_field('mfd_ukr_secondblock_map_image')): ?>
        <a href="<?php if (get_field('mfd_ukr_secondblock_map_image_link')){ the_field('mfd_ukr_secondblock_map_image_link');} ?>" target="_blank">
            <section class="map-section">
                <div class="map-wrapper">
                    <div class="map" style="background-image:url(<?php the_field('mfd_ukr_secondblock_map_image'); ?>)"></div>
                    <p class="map-description">
                        <?php if (get_field('mfd_ukr_secondblock_map_text')): ?>                
                            <?php the_field('mfd_ukr_secondblock_map_text'); ?>   
                        <?php endif; ?> 
                    </p>
                </div>
            </section>
        </a>
        <?php endif; ?>

        <!---------------CLEVER PARK OFFICE BUILDING----------------->
        <div class="business-main-container">
            <h1 class="section-title">
                <?php if (get_field('mfd_bel_title')): ?>                
                    <?php the_field('mfd_bel_title'); ?>   
                <?php endif; ?> 
            </h1>
            <div class="business-container">
                <p class="business-title"> 
                    <?php if (get_field('mfd_bel_firstblock_title')): ?>                
                        <?php the_field('mfd_bel_firstblock_title'); ?>   
                    <?php endif; ?> 
                </p>
                <div class="swiper-container-Bel1">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_firstblock_images')): ?> 
                            <?php while (have_rows('mfd_bel_firstblock_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_firstblock_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_firstblock_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron-right"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="slide-counter">
                    <div class="slide-counter-wrap">
                        <div class="swiper-button-prev-bel1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                        <div class="slide-counter-number">
                            <div class="swiper-pagination-real-1"></div>
                        </div>
                        <div class="swiper-button-next-bel1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
                    </div>
                </div>
                <div class="business-chevron-descktop"></div>
                
                <div class="business-details">
                    <?php if (have_rows('mfd_bel_firstblock_details')): ?> 
                        <?php while (have_rows('mfd_bel_firstblock_details')) : the_row(); ?> 
                            <div class="business-details-item cols3">
                                <p class="business-details-item-title">
                                    <?php if (get_sub_field('mfd_bel_firstblock_details_title')): ?>
                                        <?php the_sub_field('mfd_bel_firstblock_details_title'); ?>
                                    <?php endif; ?>
                                </p>
                                <p class="business-details-item-description">
                                    <?php if (get_sub_field('mfd_bel_firstblock_details_desc')): ?>
                                        <?php the_sub_field('mfd_bel_firstblock_details_desc'); ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        <?php endwhile; ?> 
                    <?php endif; ?> 

                </div>

                <div class="business-option-wrapper immovables">
                    <p class="business-option-description immovables">
                        <?php if (get_field('mfd_bel_firstblock_desc_top')): ?>
                            <?php the_field('mfd_bel_firstblock_desc_top'); ?>
                        <?php endif; ?>
                    </p>
                </div>

                <div class="real-estate-description">
                    <?php if (get_field('mfd_bel_firstblock_desc_bottom')): ?>
                        <?php the_field('mfd_bel_firstblock_desc_bottom'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="business-container">
                <div class="swiper-container-BelInterior1">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_firstblock_second_images')): ?> 
                            <?php while (have_rows('mfd_bel_firstblock_second_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_firstblock_second_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_firstblock_second_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="slide-counter">
                    <div class="slide-counter-wrap">
                        <div class="swiper-button-prev-belInt1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                        <div class="slide-counter-number">
                            <div class="swiper-pagination-real-2"></div>
                        </div>
                        <div class="swiper-button-next-belInt1 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
                    </div>
                </div>
                <div class="business-chevron-descktop big"></div>


                <div class="real-estate-description">
                    <?php if (get_field('mfd_bel_firstblock_second_desc_bottom')): ?>
                        <?php the_field('mfd_bel_firstblock_second_desc_bottom'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if (get_field('mfd_bel_firstblock_map_image')): ?>
        <a href="<?php if (get_field('mfd_bel_firstblock_map_image_link')){ the_field('mfd_bel_firstblock_map_image_link');} ?>" target="_blank">
            <section class="map-section">
                <div class="map-wrapper">
                    <div class="map" style="background-image:url(<?php the_field('mfd_bel_firstblock_map_image'); ?>)"></div>
                    <p class="map-description">
                        <?php if (get_field('mfd_bel_firstblock_map_text')): ?>                
                            <?php the_field('mfd_bel_firstblock_map_text'); ?>   
                        <?php endif; ?> 
                    </p>
                </div>
            </section>
        </a>    
        <?php endif; ?>

        <!---------------CLEVER PARK RESEDENTIAL----------------->
        <div class="business-main-container">
            <div class="business-container">
                <p class="business-title"> 
                    <?php if (get_field('mfd_bel_secondblock_title')): ?>                
                        <?php the_field('mfd_bel_secondblock_title'); ?>   
                    <?php endif; ?> 
                </p>
                <div class="swiper-container-Bel3">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_secondblock_images')): ?> 
                            <?php while (have_rows('mfd_bel_secondblock_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_secondblock_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_secondblock_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron-right"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="business-chevron-descktop"></div>
                <div class="business-details">
                    
                    <?php if (have_rows('mfd_bel_secondblock_details')): ?> 
                        <?php while (have_rows('mfd_bel_secondblock_details')) : the_row(); ?> 
                            <div class="business-details-item cols3">
                                <p class="business-details-item-title">
                                    <?php if (get_sub_field('mfd_bel_secondblock_details_title')): ?>
                                        <?php the_sub_field('mfd_bel_secondblock_details_title'); ?>
                                    <?php endif; ?>
                                </p>
                                <p class="business-details-item-description">
                                    <?php if (get_sub_field('mfd_bel_secondblock_details_desc')): ?>
                                        <?php the_sub_field('mfd_bel_secondblock_details_desc'); ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        <?php endwhile; ?> 
                    <?php endif; ?> 

                </div>
                <div class="business-option-wrapper immovables">
                    <p class="business-option-description immovables">
                        <?php if (get_field('mfd_bel_secondblock_desc_top')): ?>
                            <?php the_field('mfd_bel_secondblock_desc_top'); ?>
                        <?php endif; ?>
                    </p>
                </div>

                <div class="real-estate-description">
                    <p>
                        <?php if (get_field('mfd_bel_secondblock_desc_bottom')): ?>
                            <?php the_field('mfd_bel_secondblock_desc_bottom'); ?>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
            <div class="business-container">
                <div class="swiper-container-Bel4">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_secondblock_second_images')): ?> 
                            <?php while (have_rows('mfd_bel_secondblock_second_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_secondblock_second_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_secondblock_second_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron-center-left"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                
                <div class="business-chevron-descktop big"></div>
                
                <div class="real-estate-description">
                    <?php if (get_field('mfd_bel_secondblock_second_desc_bottom')): ?>
                        <?php the_field('mfd_bel_secondblock_second_desc_bottom'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if (get_field('mfd_bel_secondblock_map_image')): ?>
        <a href="<?php if (get_field('mfd_bel_secondblock_map_image_link')){ the_field('mfd_bel_secondblock_map_image_link');} ?>" target="_blank">
            <section class="map-section">
                <div class="map-wrapper">
                    <div class="map" style="background-image:url(<?php the_field('mfd_bel_secondblock_map_image'); ?>)"></div>
                    <p class="map-description">
                        <?php if (get_field('mfd_bel_secondblock_map_text')): ?>                
                            <?php the_field('mfd_bel_secondblock_map_text'); ?>   
                        <?php endif; ?> 
                    </p>
                </div>
            </section>
        </a>    
        <?php endif; ?>

        <!---------------D3----------------->
        <div class="business-main-container">
            <div class="business-container">
                <p class="business-title"> 
                    <?php if (get_field('mfd_bel_thirdblock_title')): ?>                
                        <?php the_field('mfd_bel_thirdblock_title'); ?>   
                    <?php endif; ?> 
                </p>
                <div class="swiper-container-Bel2">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_thirdblock_images')): ?> 
                            <?php while (have_rows('mfd_bel_thirdblock_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_thirdblock_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_thirdblock_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron-right"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="slide-counter">
                    <div class="slide-counter-wrap">
                        <div class="swiper-button-prev-bel2 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                        <div class="slide-counter-number">
                            <div class="swiper-pagination-real-3"></div>
                        </div>
                        <div class="swiper-button-next-bel2 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
                    </div>
                </div>


                <div class="business-chevron-descktop"></div>
                <div class="business-details">
                    <?php if (have_rows('mfd_bel_thirdblock_details')): ?> 
                        <?php while (have_rows('mfd_bel_thirdblock_details')) : the_row(); ?> 
                            <div class="business-details-item cols3">
                                <p class="business-details-item-title">
                                    <?php if (get_sub_field('mfd_bel_thirdblock_details_title')): ?>
                                        <?php the_sub_field('mfd_bel_thirdblock_details_title'); ?>
                                    <?php endif; ?>
                                </p>
                                <p class="business-details-item-description">
                                    <?php if (get_sub_field('mfd_bel_thirdblock_details_desc')): ?>
                                        <?php the_sub_field('mfd_bel_thirdblock_details_desc'); ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        <?php endwhile; ?> 
                    <?php endif; ?> 
                </div>

                <div class="business-option-wrapper immovables">
                    <p class="business-option-description immovables">
                        <?php if (get_field('mfd_bel_thirdblock_desc_top')): ?>
                            <?php the_field('mfd_bel_thirdblock_desc_top'); ?>
                        <?php endif; ?>
                    </p>
                    <button class="business-option-button">
                        <?php if (get_field('mfd_bel_thirdblock_link')): ?>
                        <a href="<?php the_field('mfd_bel_thirdblock_link'); ?>" class="business-option-button-link">
                            <?php pll_e('Site'); ?>
                        </a>
                        <?php endif; ?>
                    </button>
                </div>

                <div class="real-estate-description"><p>
                        <?php if (get_field('mfd_bel_thirdblock_desc_bottom')): ?>
                            <?php the_field('mfd_bel_thirdblock_desc_bottom'); ?>
                        <?php endif; ?>
                    </p></div>
            </div>
            <div class="business-container">
                <div class="swiper-container-BelInterior2">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_thirdblock_second_images')): ?> 
                            <?php while (have_rows('mfd_bel_thirdblock_second_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_thirdblock_second_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_thirdblock_second_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron-center-left"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="slide-counter">
                    <div class="slide-counter-wrap">
                        <div class="swiper-button-prev-belInt2 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                        <div class="slide-counter-number">
                            <div class="swiper-pagination-real-5"></div>
                        </div>
                        <div class="swiper-button-next-belInt2 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
                    </div>
                </div>
                <div class="business-chevron-descktop big"></div>

                <div class="real-estate-description">
                    <?php if (get_field('mfd_bel_thirdblock_second_desc_bottom')): ?>
                        <?php the_field('mfd_bel_thirdblock_second_desc_bottom'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="business-container">
                <div class="swiper-container-BelInterior3">
                    <div class="swiper-wrapper">
                        <?php if (have_rows('mfd_bel_thirdblock_third_images')): ?> 
                            <?php while (have_rows('mfd_bel_thirdblock_third_images')) : the_row(); ?> 
                                <div class="swiper-slide swiper-slide-business">
                                    <?php if (get_sub_field('mfd_bel_thirdblock_third_image')): ?>
                                        <div class="business-image-wrapper"
                                             style="background-image: url('<?php the_sub_field('mfd_bel_thirdblock_third_image'); ?>')">
                                        </div>
                                    <?php endif; ?> 
                                    <div class="business-chevron-center-right"></div>
                                </div>
                            <?php endwhile; ?> 
                        <?php endif; ?> 
                    </div>
                </div>
                <div class="slide-counter">
                    <div class="slide-counter-wrap">
                        <div class="swiper-button-prev-belInt3 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white__arrow_left.png'); ?>)"></div>
                        <div class="slide-counter-number">
                            <div class="swiper-pagination-real-6"></div>
                        </div>
                        <div class="swiper-button-next-belInt3 swiper-main-button slider-white-arrow" style="background-image: url(<?php echo home_url('/wp-content/themes/unicornhld/img/white_arrow_right.png'); ?>)"></div>
                    </div>
                </div>
                <div class="business-chevron-descktop big"></div>

                <div class="real-estate-description">
                    <?php if (get_field('mfd_bel_thirdblock_third_desc_bottom')): ?>
                        <?php the_field('mfd_bel_thirdblock_third_desc_bottom'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if (get_field('mfd_bel_thirdblock_map_image')): ?>
        <a href="<?php if (get_field('mfd_bel_thirdblock_map_image_link')){ the_field('mfd_bel_thirdblock_map_image_link');} ?>" target="_blank">
            <section class="map-section">
                <div class="map-wrapper">
                    <div class="map" style="background-image:url(<?php the_field('mfd_bel_thirdblock_map_image'); ?>)"></div>
                    <p class="map-description">
                        <?php if (get_field('mfd_bel_thirdblock_map_text')): ?>                
                            <?php the_field('mfd_bel_thirdblock_map_text'); ?>   
                        <?php endif; ?> 
                    </p>
                </div>
            </section>
        </a>
        <?php endif; ?>
    </section>
</div>
<?php get_footer(); ?>