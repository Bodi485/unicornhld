<?php get_header(); ?>
<div class="basic-container page-wrapper">
    <?php if (get_field('it_main_image')): ?>
        <section class="danit-first-section">
            <div class="main-image-wrapper blue">
                <div class="main-image main-image-black centered"
                     style="background-image: url(<?php the_field('it_main_image'); ?>);">
                    <div class="main-title">
                        <p>
                            <?php if (get_field('it_main_image_title')): ?>                
                                <?php the_field('it_main_image_title'); ?>   
                            <?php endif; ?> 
                        </p>
                        <?php if (get_field('it_main_image_link')): ?>  
                            <a href="<?php the_field('it_main_image_link'); ?>" class="main-link"><?php pll_e('Site'); ?></a>
                        <?php endif; ?> 
                    </div>
                </div>
            </div>
            <p class="section-text"> 
                <?php if (get_field('it_main_image_desc')): ?>                
                    <?php the_field('it_main_image_desc'); ?>   
                <?php endif; ?> 
            </p>
        </section>
    <?php endif; ?>
    <section>
        <h2 class="section-title">
            <?php if (get_field('it_benefits_title')): ?>                
                <?php the_field('it_benefits_title'); ?>   
            <?php endif; ?> 
        </h2>
        <div class="icon-wrapper">
            <?php if (have_rows('it_benefits')): ?> 
                <?php while (have_rows('it_benefits')) : the_row(); ?> 
                    <div class="icon-item">
                        <?php if (get_sub_field('it_benefits_icon')): ?>
                            <div class="icon-img" style="background-image:url(<?php the_sub_field('it_benefits_icon'); ?>)"></div>
                        <?php endif; ?> 
                        <p class="icon-info">
                            <?php if (get_sub_field('it_benefits_text')): ?>
                                <?php the_sub_field('it_benefits_text'); ?>
                            <?php endif; ?> 
                        </p>
                    </div>
                <?php endwhile; ?> 
            <?php endif; ?> 

        </div>
    </section>
    <section class="programs">
        <h2 class="section-title">
            <?php if (get_field('it_programs_title')): ?>                
                <?php the_field('it_programs_title'); ?> 
            <?php endif; ?> 
        </h2>
        <p class="section-subtitle">
            <?php if (get_field('it_programs_subtitle')): ?>                
                <?php the_field('it_programs_subtitle'); ?>   
            <?php endif; ?> 
        </p>
        <div class="container">
            <div class="programs-card-wrap">
                <?php if (have_rows('it_programs')): ?> 
                    <?php while (have_rows('it_programs')) : the_row(); ?> 
                        <?php if (get_sub_field('it_programs_bg_img')): ?>
                            <div class="programs-card" style="background-image: url(<?php the_sub_field('it_programs_bg_img'); ?>);">
                                <div class="programs-bg"></div>
                                <h6 class="programs-info-header">
                                <?php if (the_sub_field('it_programs_bg_title')): ?>                
                                   <?php the_sub_field('it_programs_bg_title'); ?>   
                                <?php endif; ?> 
                                </h6>
                                <p class="programs-info-description">
                                    <?php if (the_sub_field('it_programs_bg_desc')): ?>                
                                        <?php the_sub_field('it_programs_bg_desc'); ?>   
                                    <?php endif; ?> 
                                </p>
                            </div>
                        <?php endif; ?> 
                    <?php endwhile; ?> 
                <?php endif; ?> 
            </div>
        </div>
    </section>
    <?php if (get_field('it_map_image')): ?>
        <section class="map-section">
            <div class="map-wrapper danit-map-wrap">
                <a href="<?php if (get_field('it_map_image_link')){ the_field('it_map_image_link');} ?>" target="_blank"><div class="map danit-map" style="background-image:url(<?php the_field('it_map_image'); ?>)"></div></a>
            </div>
        </section>
    <?php endif; ?>
</div>
<?php get_footer(); ?>