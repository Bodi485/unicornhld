<!---------------FOOTER START------------------->
<footer class="footer container">
    <h2 class="section-title contact-title"><?php pll_e('Contacts'); ?></h2>
    <div class="flex-wrapper">
        <div class="flex-item">
            <div class="footer-img" style="background-image:url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/email.png'); ?>)"></div>
            <div class="contacts-text footer-img-description"><p>email</p></div>
            <div>
             <?php if (get_field('email_first', 'option')) : ?>
                <a href="mailto:<?php the_field('email_first', 'option') ?>" class="contacts-text"><?php the_field('email_first', 'option') ?></a>
            <?php endif; ?> 
             <?php if (get_field('email_second', 'option')) : ?>
                <a href="mailto:<?php the_field('email_second', 'option') ?>" class="contacts-text"><?php the_field('email_second', 'option') ?></a>
            <?php endif; ?> 
            </div>
        </div>
        <div class="flex-item">
            <div class="footer-img" style="background-image:url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/call.png'); ?>)"></div>
            <div class="contacts-text footer-img-description"><p>call</p></div>
            <div>
            <?php if (get_field('phone_first', 'option')) : ?>
               <a href="tel:<?php the_field('phone_first', 'option') ?>" class="contacts-text" style="width: 100%;"><?php the_field('phone_first', 'option') ?></a>
            <?php endif; ?> 
            <?php if (get_field('phone_second', 'option')) : ?>
               <a href="tel:<?php the_field('phone_second', 'option') ?>" class="contacts-text" style="width: 100%;"><?php the_field('phone_second', 'option') ?></a>
            <?php endif; ?> 
            </div>
        </div>
        <div class="flex-item">
            <div class="footer-img" style="background-image:url(<?php echo home_url('/wp-content/themes/unicornhld/img/energym-mobile/location.png'); ?>)"></div>
            <div class="contacts-text footer-img-description"><p>location</p></div>
            <div><p class="contacts-text ">Kiev, Ukraine</p>
                <p class="contacts-text">Minsk, Belarus</p></div>
        </div>
    </div>
</footer>
</div>
<!---------------FOOTER END------------------->
<?php wp_footer(); ?>
</body>
</html>
